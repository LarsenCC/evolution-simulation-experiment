
public class Breed {
	
	private boolean isDead; //is the breed dead?
	private int mutation; // current mutation number
	private static int creations = 0; //how many breeds were created (static for all breeds)
	private String creator; //who is the breeds creator
	private boolean isHit; //is the breed hit/hurt?
	private int type; //which type is this breed
	
	//Constructor for the first breed
	public Breed(String creator) {
		this.isDead = false;
		this.isHit = false;
		this.mutation = 0;
		creations++;
		this.creator = creator;
		this.type = Integer.valueOf(creator);
	}
	

	//Constructor for children breeds
	public Breed(String creator, int mutation, int type) {
		this.isDead = false;
		this.isHit = false;
		this.mutation = mutation;
		creations++;
		this.creator = creator;
		this.type = type;
	}
	
	//getters and setters
	public int getMutation() {
		return this.mutation;
	}
	
	public boolean getIsDead() {
		return this.isDead;
	}
	
	public String getCreator() {
		return this.creator;
	}
	
	public Boolean getIsHit() {
		return this.isHit;
	}
	
	public int getType() {
		return this.type;
	}
	
	public int getCreations() {
		return creations;
	}
	
	public void setMutation(int mutation) {
		this.mutation = mutation;
	} 
	
	public void setIsDead(boolean isDead) {
		this.isDead = isDead;
	}
	
	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	public void setIsHit(boolean isHit) {
		this.isHit = isHit;
	}
	
}