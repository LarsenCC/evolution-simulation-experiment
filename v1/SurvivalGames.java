import java.util.Arrays;

public class SurvivalGames {
	
	public static void main(String[] breeds) {
		int startingNum = Integer.valueOf(breeds[0]); // Begining number of species
		Breed[] species = new Breed[Integer.valueOf(breeds[1])]; // Initialize array with max species
		int nature = 0; // Nature count
		int length = startingNum; // Number of species
		
		int countDead = 0;
		
		System.out.println("Number of breeds: " + startingNum);
		//System.out.println(String.valueOf(startingNum));
		
		// Fill array with parent breeds
		for (int i = 0; i < startingNum; i++) {
			int num = i + 1;
			species[i] = new Breed(String.valueOf(num));
			//System.out.println(String.valueOf("My number is: " + num));
		}
		
		printAll(species);
		System.out.println();
		
		while (true) {
			System.out.println("-----------------------------------");
			System.out.println();
			
			// Nature mutation ...
			int nMutation = getNextRandom();
			nature += nMutation;
			
			System.out.printf("Nature mutation got: %d%n", nMutation);
			System.out.printf("Current Nature points after throw: %d%n", nature);
			System.out.println();
			
			int currentLenght = length;
			
			for (int c = 0; c < currentLenght; c++) {
				int bMutation = getNextRandom();
				
				// Check if alive --> add the mutation number
				if (!species[c].getIsDead()) {
					species[c].setMutation(species[c].getMutation() + bMutation); // We add the mutation number
				}
				
				// If not dead and has the same mutation number as nature --> reproduce
				if (!species[c].getIsDead() && nature == species[c].getMutation()) {
					try {
						// create new breed --> child of the creator/parent
						species[length] = new Breed(String.valueOf(species[c].getCreator() + species[c].getType()), species[c].getMutation(), species[c].getType());
						species[c].setIsHit(false); // Set the isHit value back to false!!!
						length++;
					
					// End the program, when we run out of array length ...
					} catch (java.lang.ArrayIndexOutOfBoundsException el) {
						printAll(species);
						System.out.printf("Total breeds created: %d%n", species[0].getCreations() - 1);
						System.out.printf("Current Nature points: %d%n", nature);
						System.out.println("DEAD: " + countDead);
						System.exit(0);
					}
					
				} else {
					// Kill the breed if it is already hurt/hit
					if (species[c].getIsHit()) {
						species[c].setIsDead(true);
						countDead++;

					// Else just hit/hurt it ...
					} else {
						species[c].setIsHit(true);
					}
				}
			}
			
			printAll(species);
			
			//If all the species are dead, stop the program
			//System.out.println(species[0].getCreations() + " = " + countDead);
			if (countDead == length) {
				break;
			}
			
			countDead = 0;
		}
		
		// Print the result
		printAll(species);
		System.out.printf("Total breeds created: %d%n", species[0].getCreations());
		System.out.printf("Current Nature points: %d%n", nature);
		
	} 
	
	// Print all species
	public static void printAll(Breed[] b) {
		for (int i = 0; i < b.length; i++) {
			try {
				if (b[i].getIsDead()) {
					//System.out.printf("Index: %d of Breed %s DEAD%n", i, b[i].getCreator(), b[i].getMutation()); // we can also add: got %d and is therefore 
				} else {
					System.out.printf("Index: %d of Breed %s: %3d, hit:%b%n", i, b[i].getCreator(), b[i].getMutation(), b[i].getIsHit());
				}
			} catch (java.lang.NullPointerException e1){
				//System.out.println("Not defined");
				break;
			}
		}
	}
	
	// Choose random number
	public static int getNextRandom() {
		int random = (int) (Math.random() * 6);
	
		switch (random) {
			case 0:
				return -1;
			case 1:
			case 2:
			case 3:
			case 4:
				return 0;
			case 5:
				return 1;
		}
		
		return 0;
	}
	
}