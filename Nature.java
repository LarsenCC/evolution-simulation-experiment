import java.util.ArrayList;

public class Nature {
    
    private ArrayList<Integer> cells;
    private int cells_length;

    // Constructor
    public Nature() {
        this.cells = new ArrayList<Integer>();
        this.cells.add(0);
        this.cells_length = 1;
    }

    // Getter for cells
    public ArrayList<Integer> getCells() {
        return this.cells;
    }

    // Getter for cells_length
    public int getCells_length() {
        return this.cells_length;
    }

    // Setter for cells_length
    protected void setCells_length(int cells_length) {
        this.cells_length = cells_length;
    }

    // Print all mutation numbers of all cells
    public void printCells() {
        for (int iter : this.cells) {
            System.out.print(iter + " ");
        }
        System.out.println();
    }

    // Change all values of different cells in Nature/Organism
    public void simulateEra() {
        for (int i = 0; i < this.cells_length; i++) {
            this.cells.set(i, this.cells.get(i) + randomMutation());
        }
    }

    // Return a randomly generated value according to the rules
    private int randomMutation() {
        int random = (int) (Math.random() * 6);
		switch (random) {
			case 0:
				return -1;
			case 5:
                return 1;
            default:
                return 0;
		}
    }
}