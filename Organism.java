import java.util.ArrayList;

public class Organism extends Nature {
    
    private int species; // which species is the Organism
    private int generation; // which generation of the Ogranism is it
    private String parent; // who is the parent of this Organism
    private boolean is_hurt; // is the Organism hurt
    private boolean is_dead; // is the Organism dead
    private int streak; // how long is it on point with nature
    private final String id; // id of Organism On_Cm_Gj (n = species number, m = number of cells, j = generation number)

    private static int num_of_organism_created = 0; //

    // Constructor for first Organism
    public Organism(int species) {
        super();
		Organism(species, 1, "CREATOR");
    }

    // Constructor for other Organisms
    public Organism(int species, int generation, String parent) {
        super();
        this.species = species;
        this.generation = generation + 1;
        this.parent = parent;
        this.is_hurt = false;
        this.is_dead = false;
        this.streak = 0;
        num_of_organism_created++;
        id = "O" + species + "_C" + this.getCells_length() + "_G" + this.generation;
    }

    public void makeComplex(int nature_number) {
        this.getCells().add(nature_number);
        this.setCells_length(this.getCells_length() + 1);
    }

    public boolean getIs_hurt() {
        return this.is_hurt;
    }

    public void setIs_hurt(boolean is_hurt) {
        this.is_hurt = is_hurt;
    }

    public boolean getIs_dead() {
        return is_dead;
    }

    public void setIs_dead(boolean is_dead) {
        this.is_dead = is_dead;
    }

    public int getStreak() {
        return this.streak;
    }

    public void setStreak(int streak) {
        this.streak = streak;
    }

    public void increaseStreak() {
        this.streak++;
    }

    public String getId() {
        return this.id;
    }
}