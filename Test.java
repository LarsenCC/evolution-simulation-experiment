public class Test {

    public static void main(String[] args) {
        Nature N = new Nature();
        
        /*for(int i = 0; i < 10000; i++) {
            N.simulateEra();
            N.printCells();
        }*/

        Organism o = new Organism(1);
        o.makeComplex(0);
        System.out.println("ID: " + o.getId());
        for (int i = 0; i < 5; i++) {
            o.simulateEra();
            o.printCells();
        }
    }

}