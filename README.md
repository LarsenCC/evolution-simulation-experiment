# Evolution Simulation Experiment

## Description

## Rules

### Score

At the start of each turn Nature and every organism throw a die. Based on what they throw their mutation score changes.
If they throw 1 their mutation score is decresed by 1. If they throw 6 their mutation score increses by 1. If they throw anything else (2-5) their mutation score remains the same.

After throwing it is determined what happens to each organism. If its mutation score is the same as Natures nothing happens. However if its mutation score is different than Natures that organism becomes hurt. If in the next round that organism does not manage to tie Natures mutation score it dies. If it manages to tie its mutation score to Natures oraganism is not hurt anymore.

### Reproduction

If the organism manages to not be hurt for two rounds it becomes available for reproduction. While available for reproduction the organism can replicate itself each round with some chance. New organism has the same cell count and cells mutation scores. Its generation number is one higher from its parent. If a creature becomes hurt it is no longer available for reproduction, but can become again later.

### Complex organisms

If the organism manages to not be hurt for three rounds it has a chance to become complex. That means that the organism can create more cells for itself. If the organism becomes hurt, it no longer has a chance to become complex, but can gain that ability again if previous conditions are met.

A complex organism behaves similarly to single cell organism, except every one of his cells has its own score counter. We than take a closer look at mutation score of each cell in an organism to determine whether it becomes hurt, dies, can reproduce or becomes even more complex.

Average mutation score and standard deviation are calculated from cells for every complex organims. They are combined when determining organisms future path.

## Implementation

### Organism

Each organism has next parameters:

| Parameter | Type | Purpose |
| - | - | - |
| species | int | Tells which starting organism is this organisms predecessor |
| generation | int | Which generation the organism is |
| parent | String | Name of the parent (their id) |
| is_hurt | bool | If the organism is hurt |
| is_dead | bool | If the organism is dead |
| streak | int | Organisms streak with the Nature |
| cells | int ArrayList | Holds scores for every cell in an organism |
| cells_length | int | Tells how many cells an organism has |
| id | String | Organisms name (Ox_Cy_Gz; x ~ species, y ~ number of cells, z ~ generation) |

and methods:

| Name | Type | Purpose |
| - | - | - |
| simulateEra | void | It changes each cells mutation score for an organism |
| randomMutation | int | Returns how mutation score changes |
| makeComplex | void | Adds another cell to an organism, its score becomes Natures |